$(document).ready(function() {
  $(".section3__slider").slick({
    slidesToShow: 3,
    dots: true,
    prevArrow:
      false,
    nextArrow:false,
    autoplay:true,
    autoplaySpeed: 1000     
  });
});

jQuery(window).scroll(startCounter);
function startCounter() {
    var hT = jQuery('.section4__box').offset().top,
        hH = jQuery('.section4__box').outerHeight(),
        wH = jQuery(window).height();
    if (jQuery(window).scrollTop() > hT+hH-wH) {
        jQuery(window).off("scroll", startCounter);
        jQuery('#counter').each(function () {
            var $this = jQuery(this);
            jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
                duration: 2000,
                easing: 'swing',
                step: function () {
                    $this.text(Math.ceil(this.Counter));
                }
            });
        });
    }
}
// $(function(){
//   $(".adaptiveNav").on('click', function() {
//      $(".Navbar__Items").show();
//   });
// });

//'<div class="section3__slider__arrow" ><img class="section3__arrow__image" src="images/arrow--right.png"></div>'
// $('.count').each(function () {
//   $(this).prop('Counter',0).animate({
//           Counter: $(this).text()
//   },
//    {
//     duration: 3000,
//     easing: 'swing',
//     step: function (now) {
//       $(this).text(Math.ceil(now));
//     }
//   });
// });
// var a = 0;
// $(window).scroll(function() {

//   $(window).scroll(startCounter);
//   function startCounter() {
//       if ($(window).scrollTop() > 200) {
//           $(window).off("scroll", startCounter);
//           $('#counter').each(function () {
//               var $this = $(this);
//               jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
//                   duration: 1000,
//                   easing: 'swing',
//                   step: function () {
//                       $this.text(Math.ceil(this.Counter));
//                   }
//               });
//           });
//       }
//   }
